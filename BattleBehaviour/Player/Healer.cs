﻿using System;
using System.Collections.Generic;
using UnityEngine;
using BAYUAjiStudio.Battle;
using BAYUAjiStudio.AI.Pathfinding;
using BAYUAjiStudio.Stat;
namespace BAYUAjiStudio.AI.Behaviour
{
    public class Healer : ArtificialBehaviour
    {
        CharacterController charactercontroller;
        movement_stat mov_stat;
        Animator animator;
        Transformnode transformnode;
        GameObject[] teammates;
        GameObject enemy;

        public int Run;
        public int Attack;
        public int Healing;
        public int Masshealing;
        public int Idle;
        void Start()
        {
            charactercontroller = GetComponent<CharacterController>();
            mov_stat = GetComponent<movement_stat>();
            animator = GetComponent<Animator>();
            transformnode = GetComponent<Transformnode>();
        }
        void Update()
        {
            if (BattlegroundNode)
            {
                if (!issearchcomplete)
                {
                    blindsearch();
                }
            }
        }
        void blindsearch()
        {
            BattlegroundNode.blindsearch();
        }
        void move()
        {
            Vector3 lookpos = next().transform.position - transform.position;
            lookpos.y = 0;
            Quaternion desirerotation = Quaternion.LookRotation(lookpos);
            transform.rotation = Quaternion.Slerp(transform.rotation, desirerotation, mov_stat.rotation_speed * Time.deltaTime);
            //movetoward
            Vector3 movement = transform.TransformDirection(new Vector3(0, -1, 1 * mov_stat.speed * Time.deltaTime));
            charactercontroller.Move(movement);
        }
        GameObject woundedteammate
        {
            get
            {
                return null;
            }
        }
        void lockon()
        {
            Vector3 lookpos = woundedteammate.transform.position - transform.position;
            lookpos.y = 0;
            Quaternion desirerotation = Quaternion.LookRotation(lookpos);
            transform.rotation = Quaternion.Slerp(transform.rotation, desirerotation, mov_stat.rotation_speed * Time.deltaTime);
        }
        void gotowoundedteammates()
        {
            Vector3 lookpos = woundedteammate.transform.position - transform.position;
            lookpos.y = 0;
            Quaternion desirerotation = Quaternion.LookRotation(lookpos);
            transform.rotation = Quaternion.Slerp(transform.rotation, desirerotation, mov_stat.rotation_speed * Time.deltaTime);
            //movetoward
            Vector3 movement = transform.TransformDirection(new Vector3(0, -1, 1 * mov_stat.speed * Time.deltaTime));
            charactercontroller.Move(movement);
        }
        BattleGround battleground
        {
            get { return BattleGround.CurrentBattleGround; }
        }
        node Currentnode
        {
            get { if (transformnode) return transformnode.Currentnode(); return null; }
        }
        node BattlegroundNode
        {
            get { if (battleground) return battleground.battleground; return null; }
        }
        node next()
        {
            node leastfscore = Currentnode;
            foreach (node neightbor in leastfscore.Neightbor)
            {
                if (fscore[neightbor] < fscore[leastfscore])
                {
                    leastfscore = neightbor;
                }
            }
            return leastfscore;
        }
        Dictionary<node, int> fscore
        {
            get { return BattlegroundNode.Fscore(); }
        }
        bool issearchcomplete
        {
            get { return BattlegroundNode.IsSearchcomplete(); }
        }
        bool isenemynearest()
        {
            foreach (GameObject i in battleground.Enemies)
            {
                if (i.GetComponent<Collider>().bounds.SqrDistance(transform.position) < 10f)
                {
                    enemy = i;
                    return true;
                }
            }
            enemy = battleground.Enemyofplayer(gameObject);
            return false;
        }
        bool isenemyonmeleerange()
        {
            foreach (GameObject i in battleground.Enemies)
            {
                if (i.GetComponent<Collider>().bounds.SqrDistance(transform.position) < 1f)
                {
                    enemy = i;
                    return true;
                }
            }
            enemy = battleground.Enemyofplayer(gameObject);
            return false;
        }
        bool isteammateswounded()
        {
            foreach(GameObject i in teammates)
            {
                //if(i.health<maxhealth)
                //return true
            }
            return false;
        }
        bool isteammateonhealingrange()
        {
            return woundedteammate.GetComponent<Collider>().bounds.SqrDistance(transform.position) < 10f;
        }
        public override void BeginBehaviour()
        {
            teammates = GameObject.FindGameObjectsWithTag("Player");
        }
        public override void UpdateBehaviour()
        {
            if (Currentnode && BattlegroundNode)
            {
                if (Currentnode != BattlegroundNode)
                {
                    if (issearchcomplete)
                    {
                        move();
                        animator.SetInteger("Behaviour", Run);//assaultbattleground
                    }
                    else
                    {
                        animator.SetInteger("Behaviour", Idle);//wait
                    }
                }
                else
                {
                    if (isenemyonmeleerange())
                    {
                        animator.SetInteger("Behaviour", Attack);
                    }
                    else
                    {
                        if (isteammateswounded())
                        {
                            if (isteammateonhealingrange())
                            {
                                lockon();
                                animator.SetInteger("Behaviour", Healing);
                            }
                            else
                            {
                                gotowoundedteammates();
                                animator.SetInteger("Behaviour", Run);
                            }
                        }
                        else
                        {
                            //getbehindteammates
                            animator.SetInteger("Behaviour",Idle);//temp
                        }
                    }
                }
            }
            else
            {
                animator.SetInteger("Behaviour", Idle);
            }
        }
        public override void EndBehaviour()
        {
            
        }
    }
}
