﻿using System;
using System.Collections.Generic;
using UnityEngine;
using BAYUAjiStudio.AI.Pathfinding;
using BAYUAjiStudio.Battle;
using BAYUAjiStudio.Stat;
namespace BAYUAjiStudio.AI.Behaviour
{
    /// <summary>
    /// Combination heavy attack with positionattack
    /// </summary>
    public class HeavyAttack2 : ArtificialBehaviour
    {
        CharacterController charactercontroller;
        movement_stat mov_stat;
        Animator animator;
        Transformnode transformnode;
        Quaternion attackposition;
        float attackradius;
        Vector3 clockwisetarget;
        Vector3 counterclockwisetarget;
        Vector3 clockwise;
        Vector3 counterclockwise;
        GameObject[] enemiesinmeleerange;
        int count;

        public Vector3 AttackPosition;
        [Range(0f, 180f)] public float PositionTolerance;

        public int Run;
        public int Stop;
        public int Attack;
        public int MassAttack;

        void Start()
        {
            charactercontroller = GetComponent<CharacterController>();
            mov_stat = GetComponent<movement_stat>();
            animator = GetComponent<Animator>();
            transformnode = GetComponent<Transformnode>();
            enemiesinmeleerange = new GameObject[3];
            attackposition = Quaternion.LookRotation(AttackPosition);
            attackradius = AttackPosition.magnitude;
            clockwise = new Vector3(AttackPosition.z, AttackPosition.y, -AttackPosition.x);
            counterclockwise = new Vector3(-AttackPosition.z, AttackPosition.y, AttackPosition.x);
        }
        void Update()
        {
            if (BattlegroundNode)
            {
                if (!issearchcomplete)
                {
                    blindsearch();
                }
            }
        }
        void blindsearch()
        {
            BattlegroundNode.blindsearch();
        }
        void move()
        {
            Vector3 lookpos = next().transform.position - transform.position;
            lookpos.y = 0;
            Quaternion desirerotation = Quaternion.LookRotation(lookpos);
            transform.rotation = Quaternion.Slerp(transform.rotation, desirerotation, mov_stat.rotation_speed * Time.deltaTime);
            //movetoward
            Vector3 movement = transform.TransformDirection(new Vector3(0, -1, 1 * mov_stat.speed * Time.deltaTime));
            charactercontroller.Move(movement);
        }
        void movetowardenemy()
        {
            Vector3 lookpos = enemy.transform.position - transform.position;
            lookpos.y = 0;
            Quaternion desirerotation = Quaternion.LookRotation(lookpos);
            transform.rotation = Quaternion.Slerp(transform.rotation, desirerotation, mov_stat.rotation_speed * Time.deltaTime);
            //movetoward
            Vector3 movement = transform.TransformDirection(new Vector3(0, -1, 1 * mov_stat.speed * Time.deltaTime));
            charactercontroller.Move(movement);
        }
        void movetowardattacklocation()
        {
            Vector3 attacklocation = enemy.transform.position + AttackPosition;
            attacklocation = Currentnode.bounding(attacklocation);
            Vector3 lookpos = attacklocation - transform.position;
            lookpos.y = 0;
            Quaternion desirerotation = Quaternion.LookRotation(lookpos);
            transform.rotation = Quaternion.Slerp(transform.rotation, desirerotation, 10 * mov_stat.rotation_speed * Time.deltaTime);
            //movetoward
            Vector3 movement = transform.TransformDirection(new Vector3(0, -1, 1 * mov_stat.speed * Time.deltaTime));
            charactercontroller.Move(movement);
        }
        void circlearoundenemyclockwise()
        {
            Vector3 target = Currentnode.bounding(clockwisetarget);
            Vector3 lookpos = target - transform.position;
            lookpos.y = 0;

            Quaternion desirerotation = Quaternion.LookRotation(lookpos);
            transform.rotation = Quaternion.Slerp(transform.rotation, desirerotation, mov_stat.rotation_speed * Time.deltaTime);
            //movetoward
            Vector3 movement = transform.TransformDirection(new Vector3(0, -1, 1 * mov_stat.speed * Time.deltaTime));
            charactercontroller.Move(movement);
        }
        void circlearoundenemycounterclockwise()
        {
            Vector3 target = Currentnode.bounding(counterclockwisetarget);
            Vector3 lookpos = target - transform.position;
            lookpos.y = 0;

            Quaternion desirerotation = Quaternion.LookRotation(lookpos);
            transform.rotation = Quaternion.Slerp(transform.rotation, desirerotation, mov_stat.rotation_speed * Time.deltaTime);
            //movetoward
            Vector3 movement = transform.TransformDirection(new Vector3(0, -1, 1 * mov_stat.speed * Time.deltaTime));
            charactercontroller.Move(movement);
        }
        void lockon()
        {
            Vector3 lookpos = enemy.transform.position - transform.position;
            lookpos.y = 0;
            Quaternion desirerotation = Quaternion.LookRotation(lookpos);
            transform.rotation = Quaternion.Slerp(transform.rotation, desirerotation, mov_stat.rotation_speed * Time.deltaTime);
        }
        BattleGround battleground
        {
            get { return BattleGround.CurrentBattleGround; }
        }
        node Currentnode
        {
            get { if (transformnode) return transformnode.Currentnode(); return null; }
        }
        node BattlegroundNode
        {
            get { if (battleground) return battleground.battleground; return null; }
        }
        bool issearchcomplete
        {
            get { return BattlegroundNode.IsSearchcomplete(); }
        }
        bool isinattackposition()
        {
            Vector3 diffence = transform.position - enemy.transform.position;
            Quaternion differencerotation = Quaternion.LookRotation(diffence);
            return (Quaternion.Angle(differencerotation, attackposition) < PositionTolerance);
        }
        bool isattacklocationnearest()
        {
            Vector3 diffence = transform.position - enemy.transform.position;
            Quaternion differencerotation = Quaternion.LookRotation(diffence);

            return (Quaternion.Angle(differencerotation, attackposition) < 90f);
        }
        bool isclockwiseeficient()
        {
            clockwisetarget = enemy.transform.position + clockwise;

            counterclockwisetarget = enemy.transform.position + counterclockwise;

            float clockwisedistance = Vector3.Distance(transform.position, clockwisetarget);
            float counterclockwisedistance = Vector3.Distance(transform.position, counterclockwisetarget);

            return clockwisedistance < counterclockwisedistance;
        }
        node next()
        {
            node leastfscore = Currentnode;
            foreach (node neightbor in leastfscore.Neightbor)
            {
                if (fscore[neightbor] < fscore[leastfscore])
                {
                    leastfscore = neightbor;
                }
            }
            return leastfscore;
        }
        bool isenemyinmeleerange()
        {
            count = 0;
            foreach (GameObject i in battleground.Enemies)
            {
                if (i.GetComponent<Collider>().bounds.SqrDistance(transform.position) < 1f)
                {
                    enemiesinmeleerange[count] = i;
                    count++;if (count == enemiesinmeleerange.Length) break;
                }
            }
            return count > 0;
        }
        GameObject enemy
        {
            get{
                if(count>0)
                {
                    return enemiesinmeleerange[0];
                }
                return battleground.Enemyofplayer(gameObject);
            }
        }
        Dictionary<node, int> fscore
        {
            get { return BattlegroundNode.Fscore(); }
        }
        public override void BeginBehaviour()
        {
            
        }
        public override void UpdateBehaviour()
        {
            if (Currentnode && BattlegroundNode)
            {
                if (Currentnode != BattlegroundNode)
                {
                    if (issearchcomplete)
                    {
                        move();
                        animator.SetInteger("Behaviour", Run);//assaultbattleground
                    }
                    else
                    {
                        animator.SetInteger("Behaviour", Stop);//wait
                    }
                }
                else
                {
                    if (isenemyinmeleerange())
                    {
                        if (count == enemiesinmeleerange.Length)
                        {
                            animator.SetInteger("Behaviour", MassAttack);
                        }
                        else
                        {
                            lockon();
                            animator.SetInteger("Behaviour", Attack);
                        }
                    }
                    else
                    {
                        if (isinattackposition())
                        {
                            movetowardenemy();
                            animator.SetInteger("Behaviour", Run);
                        }
                        else
                        {
                            if (isattacklocationnearest())
                            {
                                movetowardattacklocation();
                                animator.SetInteger("Behaviour", Run);
                            }
                            else
                            {
                                if (isclockwiseeficient())
                                {
                                    circlearoundenemyclockwise();
                                    animator.SetInteger("Behaviour", Run);
                                }
                                else
                                {
                                    circlearoundenemycounterclockwise();
                                    animator.SetInteger("Behaviour", Run);
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                animator.SetInteger("Behaviour", Stop);
            }
        }
        public override void EndBehaviour()
        {
            
        }

    }
}
