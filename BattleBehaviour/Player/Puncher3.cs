﻿using System;
using System.Collections.Generic;
using UnityEngine;
using BAYUAjiStudio.Battle;
using BAYUAjiStudio.AI.Pathfinding;
using BAYUAjiStudio.Stat;
namespace BAYUAjiStudio.AI.Behaviour
{
    /// <summary>
    /// Alternatename is PositionPuncher
    /// Combination of PositionAttack and puncher behaviour
    /// </summary>
    public class Puncher3 : ArtificialBehaviour
    {
        CharacterController charactercontroller;
        movement_stat mov_stat;
        Animator animator;
        Transformnode transformnode;
        Quaternion attackposition;
        float attackradius;
        Vector3 clockwisetarget;
        Vector3 counterclockwisetarget;
        Vector3 clockwise;
        Vector3 counterclockwise;
        GameObject enemy;

        public Vector3 AttackPosition;
        [Range(0f, 180f)] public float PositionTolerance;

        public int Run;
        public int Attack;
        public int RangeAttack;
        public int Idle;
        void Start()
        {
            charactercontroller = GetComponent<CharacterController>();
            mov_stat = GetComponent<movement_stat>();
            animator = GetComponent<Animator>();
            transformnode = GetComponent<Transformnode>();
            attackposition = Quaternion.LookRotation(AttackPosition);
            attackradius = AttackPosition.sqrMagnitude;
            clockwise = new Vector3(AttackPosition.z, AttackPosition.y, -AttackPosition.x);
            counterclockwise= new Vector3(-AttackPosition.z, AttackPosition.y, AttackPosition.x);
        }
        void Update()
        {
            if (BattlegroundNode)
            {
                if (!issearchcomplete)
                {
                    blindsearch();
                }
            }
        }
        void blindsearch()
        {
            BattlegroundNode.blindsearch();
        }
        void move()
        {
            Vector3 lookpos = next().transform.position - transform.position;
            lookpos.y = 0;
            Quaternion desirerotation = Quaternion.LookRotation(lookpos);
            transform.rotation = Quaternion.Slerp(transform.rotation, desirerotation, mov_stat.rotation_speed * Time.deltaTime);
            //movetoward
            Vector3 movement = transform.TransformDirection(new Vector3(0, -1, 1 * mov_stat.speed * Time.deltaTime));
            charactercontroller.Move(movement);
        }
        void lockon()
        {
            Vector3 lookpos = enemy.transform.position - transform.position;
            lookpos.y = 0;
            Quaternion desirerotation = Quaternion.LookRotation(lookpos);
            transform.rotation = Quaternion.Slerp(transform.rotation, desirerotation, mov_stat.rotation_speed * Time.deltaTime);
        }
        void movetowardenemy()
        {
            Vector3 lookpos = enemy.transform.position - transform.position;
            lookpos.y = 0;
            Quaternion desirerotation = Quaternion.LookRotation(lookpos);
            transform.rotation = Quaternion.Slerp(transform.rotation, desirerotation, 10*mov_stat.rotation_speed * Time.deltaTime);
            //movetoward
            Vector3 movement = transform.TransformDirection(new Vector3(0, -1, 1 * mov_stat.speed * Time.deltaTime));
            charactercontroller.Move(movement);
        }
        void movetowardattacklocation()
        {
            Vector3 attacklocation=enemy.transform.position+AttackPosition;
            attacklocation = Currentnode.bounding(attacklocation);
            Vector3 lookpos = attacklocation - transform.position;
            lookpos.y = 0;
            Quaternion desirerotation = Quaternion.LookRotation(lookpos);
            transform.rotation = Quaternion.Slerp(transform.rotation, desirerotation, 10 * mov_stat.rotation_speed * Time.deltaTime);
            //movetoward
            Vector3 movement = transform.TransformDirection(new Vector3(0, -1, 1 * mov_stat.speed * Time.deltaTime));
            charactercontroller.Move(movement);
        }
        void circlearoundenemyclockwise()
        {
            Vector3 target = Currentnode.bounding(clockwisetarget);
            Vector3 lookpos = target - transform.position;
            lookpos.y = 0;

            Quaternion desirerotation = Quaternion.LookRotation(lookpos);
            transform.rotation = Quaternion.Slerp(transform.rotation, desirerotation, mov_stat.rotation_speed * Time.deltaTime);
            //movetoward
            Vector3 movement = transform.TransformDirection(new Vector3(0, -1, 1 * mov_stat.speed * Time.deltaTime));
            charactercontroller.Move(movement);
        }
        void circlearoundenemycounterclockwise()
        {
            Vector3 target = Currentnode.bounding(counterclockwisetarget);
            Vector3 lookpos = target - transform.position;
            lookpos.y = 0;

            Quaternion desirerotation = Quaternion.LookRotation(lookpos);
            transform.rotation = Quaternion.Slerp(transform.rotation, desirerotation, mov_stat.rotation_speed * Time.deltaTime);
            //movetoward
            Vector3 movement = transform.TransformDirection(new Vector3(0, -1, 1 * mov_stat.speed * Time.deltaTime));
            charactercontroller.Move(movement);
        }
        BattleGround battleground
        {
            get { return BattleGround.CurrentBattleGround; }
        }
        node Currentnode
        {
            get { if (transformnode) return transformnode.Currentnode(); return null; }
        }
        node BattlegroundNode
        {
            get { if (battleground) return battleground.battleground; return null; }
        }
        node next()
        {
            node leastfscore = Currentnode;
            foreach (node neightbor in leastfscore.Neightbor)
            {
                if (fscore[neightbor] < fscore[leastfscore])
                {
                    leastfscore = neightbor;
                }
            }
            return leastfscore;
        }
        bool isenemyinmeleerange()
        {
            foreach (GameObject i in battleground.Enemies)
            {
                if (i.GetComponent<Collider>().bounds.SqrDistance(transform.position) < 1f)
                {
                    enemy = i;
                    return true;
                }
            }
            enemy = battleground.Enemyofplayer(gameObject);
            return false;
        }
        Dictionary<node, int> fscore
        {
            get { return BattlegroundNode.Fscore(); }
        }
        bool issearchcomplete
        {
            get { return BattlegroundNode.IsSearchcomplete(); }
        }
        bool isattacklocationnearest()
        {
            Vector3 diffence = transform.position - enemy.transform.position;
            Quaternion differencerotation = Quaternion.LookRotation(diffence);

            return (Quaternion.Angle(differencerotation, attackposition) < 90f);
        }
        bool isinattackposition()
        {
            Vector3 diffence = transform.position-enemy.transform.position;
            Quaternion differencerotation = Quaternion.LookRotation(diffence);

            return (Quaternion.Angle(differencerotation, attackposition) < PositionTolerance);
        }
        bool isenemyinrangeattack()
        {
            return enemy.GetComponent<Collider>().bounds.SqrDistance(transform.position) < attackradius;
        }
        bool isclockwiseeficient()
        {
            clockwisetarget = enemy.transform.position + clockwise;

            counterclockwisetarget = enemy.transform.position + counterclockwise;

            float clockwisedistance = Vector3.Distance(transform.position, clockwisetarget);
            float counterclockwisedistance = Vector3.Distance(transform.position, counterclockwisetarget);

            return clockwisedistance < counterclockwisedistance;
        }
        public override void BeginBehaviour()
        {
        }
        public override void UpdateBehaviour()
        {
            if (Currentnode && BattlegroundNode)
            {
                if (Currentnode != BattlegroundNode)
                {
                    if (issearchcomplete)
                    {
                        move();
                        animator.SetInteger("Behaviour", Run);//assaultbattleground
                    }
                    else
                    {
                        animator.SetInteger("Behaviour", Idle);//wait
                    }
                }
                else
                {
                    if (!isenemyinmeleerange())
                    {
                        if (isinattackposition())
                        {
                            if (isenemyinrangeattack())
                            {
                                lockon();
                                animator.SetInteger("Behaviour", RangeAttack);
                            }
                            else
                            {
                                animator.SetInteger("Behaviour", Run);
                                movetowardenemy();
                            }
                        }
                        else
                        {
                            if(isattacklocationnearest())
                            {
                                movetowardattacklocation();
                                animator.SetInteger("Behaviour", Run);
                            }
                            else
                            {
                                if (isclockwiseeficient())
                                {
                                    circlearoundenemyclockwise();
                                    animator.SetInteger("Behaviour", Run);
                                }
                                else
                                {
                                    circlearoundenemycounterclockwise();
                                    animator.SetInteger("Behaviour", Run);
                                }
                            }
                        }
                    }
                    else
                    {
                        lockon();
                        animator.SetInteger("Behaviour", Attack);
                    }
                }
            }
            else
            {
                animator.SetInteger("Behaviour", Idle);
            }
        }
        public override void EndBehaviour()
        {
        }

    }
}
