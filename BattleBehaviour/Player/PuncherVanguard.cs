﻿using System;
using System.Collections.Generic;
using UnityEngine;
using BAYUAjiStudio.AI.Pathfinding;
using BAYUAjiStudio.Battle;
using BAYUAjiStudio.Stat;
namespace BAYUAjiStudio.AI.Behaviour
{
    /// <summary>
    /// combination vanguard and pucher
    /// intercept enemy is block enemy path to vital player
    /// </summary>
    public class PuncherVanguard : ArtificialBehaviour
    {
        CharacterController charactercontroller;
        movement_stat mov_stat;
        Animator animator;
        Transformnode transformnode;
        GameObject[] enemiesinperimeterrange;
        GameObject[] enemiesinmeleerange;
        int enemyinperimeterrangecount;
        int enemyinmeleerangecount;
        float targertoprotectperimeterrange;
        Vector3 globalguardpos;
        public GameObject Targettoprotect;
        public Vector3 Guardpos;

        public int Run;
        public int Attack;
        public int RangeAttack;
        public int Idle;

        void Start()
        {
            charactercontroller = GetComponent<CharacterController>();
            mov_stat = GetComponent<movement_stat>();
            animator = GetComponent<Animator>();
            transformnode = GetComponent<Transformnode>();
            enemiesinperimeterrange = new GameObject[3];
            enemiesinmeleerange = new GameObject[3];
            targertoprotectperimeterrange = Guardpos.sqrMagnitude * 2f;
        }
        void Update()
        {
            if (BattlegroundNode)
            {
                if (!issearchcomplete)
                {
                    blindsearch();
                }
            }
        }
        void blindsearch()
        {
            BattlegroundNode.blindsearch();
        }
        void move()
        {
            Vector3 lookpos = next().transform.position - transform.position;
            lookpos.y = 0;
            Quaternion desirerotation = Quaternion.LookRotation(lookpos);
            transform.rotation = Quaternion.Slerp(transform.rotation, desirerotation, mov_stat.rotation_speed * Time.deltaTime);
            //movetoward
            Vector3 movement = transform.TransformDirection(new Vector3(0, -1, 1 * mov_stat.speed * Time.deltaTime));
            charactercontroller.Move(movement);
        }
        void interceptenemy()
        {
            Vector3 lookpos = enemy.transform.position - transform.position;
            lookpos.y = 0;
            Quaternion desirerotation = Quaternion.LookRotation(lookpos);
            transform.rotation = Quaternion.Slerp(transform.rotation, desirerotation, mov_stat.rotation_speed * Time.deltaTime);
            //movetoward
            Vector3 movement = transform.TransformDirection(new Vector3(0, -1, 1 * mov_stat.speed * Time.deltaTime));
            charactercontroller.Move(movement);
        }
        void returntoguardpos()
        {
            Vector3 lookpos = globalguardpos - transform.position;
            lookpos.y = 0;
            Quaternion desirerotation = Quaternion.LookRotation(lookpos);
            transform.rotation = Quaternion.Slerp(transform.rotation, desirerotation, mov_stat.rotation_speed * Time.deltaTime);
            //movetoward
            Vector3 movement = transform.TransformDirection(new Vector3(0, -1, 1 * mov_stat.speed * Time.deltaTime));
            charactercontroller.Move(movement);
        }
        void lockon()
        {
            Vector3 lookpos = enemy.transform.position - transform.position;
            lookpos.y = 0;
            Quaternion desirerotation = Quaternion.LookRotation(lookpos);
            transform.rotation = Quaternion.Slerp(transform.rotation, desirerotation, mov_stat.rotation_speed * Time.deltaTime);
        }
        BattleGround battleground
        {
            get { return BattleGround.CurrentBattleGround; }
        }
        node Currentnode
        {
            get { if (transformnode) return transformnode.Currentnode(); return null; }
        }
        node BattlegroundNode
        {
            get { if (battleground) return battleground.battleground; return null; }
        }
        node TargettoProtectNode
        {
            get
            {
                if (Targettoprotect)
                {
                    return Targettoprotect.GetComponent<Transformnode>().Currentnode();
                }
                return null;
            }
        }
        bool issearchcomplete
        {
            get { return BattlegroundNode.IsSearchcomplete(); }
        }
        node next()
        {
            node leastfscore = Currentnode;
            foreach (node neightbor in leastfscore.Neightbor)
            {
                if (fscore[neightbor] < fscore[leastfscore])
                {
                    leastfscore = neightbor;
                }
            }
            return leastfscore;
        }
        bool isinguardpos()
        {
            globalguardpos = Targettoprotect.transform.position + Guardpos;
            globalguardpos = Currentnode.bounding(globalguardpos);
            return Vector3.Distance(globalguardpos, transform.position) < 1f;
        }
        bool isenemyinperimeterrange()
        {
            enemyinperimeterrangecount = 0;
            foreach (GameObject i in battleground.Enemies)
            {
                if (i.GetComponent<Collider>().bounds.SqrDistance(Targettoprotect.transform.position) < targertoprotectperimeterrange)
                {
                    enemiesinperimeterrange[enemyinperimeterrangecount] = i;
                    enemyinperimeterrangecount++; if (enemyinperimeterrangecount == enemiesinperimeterrange.Length) break;
                }
            }
            return enemyinperimeterrangecount > 0;
        }
        bool isenemyinmeleerange()
        {
            enemyinmeleerangecount = 0;
            foreach (GameObject i in battleground.Enemies)
            {
                if (i.GetComponent<Collider>().bounds.SqrDistance(transform.position) < 1f)
                {
                    enemiesinmeleerange[enemyinmeleerangecount] = i;
                    enemyinmeleerangecount++; if (enemyinmeleerangecount == enemiesinmeleerange.Length) break;
                }
            }
            return enemyinmeleerangecount > 0;
        }
        GameObject enemy
        {
            get
            {
                if (enemyinmeleerangecount > 0)
                {
                    return enemiesinmeleerange[0];
                }
                if (enemyinperimeterrangecount > 0)
                {
                    return enemiesinperimeterrange[0];
                }
                return battleground.Enemyofplayer(gameObject);
            }
        }
        Dictionary<node, int> fscore
        {
            get { return BattlegroundNode.Fscore(); }
        }
        public override void BeginBehaviour()
        {
        }
        public override void UpdateBehaviour()
        {
            if (Currentnode && BattlegroundNode && TargettoProtectNode)
            {
                if (Currentnode != BattlegroundNode)
                {
                    if (issearchcomplete)
                    {
                        move();
                        animator.SetInteger("Behaviour", Run);//assaultbattleground
                    }
                    else
                    {
                        animator.SetInteger("Behaviour", Idle);//wait
                    }
                }
                else
                {
                    if (Currentnode != TargettoProtectNode)
                    {
                        if (isenemyinmeleerange())
                        {
                            lockon();
                            animator.SetInteger("Behaviour", Attack);
                        }
                        else
                        {
                            interceptenemy();
                            animator.SetInteger("Behaviour", Run);
                        }
                    }
                    else
                    {
                        if (isenemyinperimeterrange())
                        {
                            if (isenemyinmeleerange())
                            {
                                lockon();
                                animator.SetInteger("Behaviour", Attack);
                            }
                            else
                            {
                                interceptenemy();
                                animator.SetInteger("Behaviour", Run);
                            }
                        }
                        else
                        {
                            if (isinguardpos())
                            {
                                lockon();
                                animator.SetInteger("Behaviour", RangeAttack);
                            }
                            else
                            {
                                returntoguardpos();
                                animator.SetInteger("Behaviour", Run);//defend perimeter
                            }
                        }
                    }
                }
            }
            else
            {
                animator.SetInteger("Behaviour", Idle);
            }
        }
        public override void EndBehaviour()
        {
        }
    }
}
