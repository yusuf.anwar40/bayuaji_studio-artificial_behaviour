﻿using System;
using System.Collections.Generic;
using UnityEngine;
using BAYUAjiStudio.AI.Pathfinding;
using BAYUAjiStudio.Battle;
using BAYUAjiStudio.Stat;

namespace BAYUAjiStudio.AI.Behaviour.Enemy
{
    /// <summary>
    /// combination waitforattack and Commando
    /// </summary>
    public class Commando2 : ArtificialBehaviour
    {
        CharacterController charactercontroller;
        movement_stat mov_stat;
        Animator animator;
        Transformnode transformnode;
        GameObject[] enemiesinrange;
        BattleGround battleground;
        int count;

        public int Run;
        public int Stop;
        public int Attack;
        public int MassAttack;
        public int RangeAttack;
        void Start()
        {
            charactercontroller = GetComponent<CharacterController>();
            mov_stat = GetComponent<movement_stat>();
            animator = GetComponent<Animator>();
            transformnode = GetComponent<Transformnode>();
            enemiesinrange = new GameObject[3];
        }
        void Update()
        {
            if (BattlegroundNode)
            {
                if (!issearchcomplete)
                {
                    blindsearch();
                }
            }
        }
        void blindsearch()
        {
            BattlegroundNode.blindsearch();
        }
        void move()
        {
            Vector3 lookpos = next().transform.position - transform.position;
            lookpos.y = 0;
            Quaternion desirerotation = Quaternion.LookRotation(lookpos);
            transform.rotation = Quaternion.Slerp(transform.rotation, desirerotation, mov_stat.rotation_speed * Time.deltaTime);
            //movetoward
            Vector3 movement = transform.TransformDirection(new Vector3(0, -1, 1 * mov_stat.speed * Time.deltaTime));
            charactercontroller.Move(movement);
        }
        void movetowardenemy()
        {
            Vector3 lookpos = enemy.transform.position - transform.position;
            lookpos.y = 0;
            Quaternion desirerotation = Quaternion.LookRotation(lookpos);
            transform.rotation = Quaternion.Slerp(transform.rotation, desirerotation, mov_stat.rotation_speed * Time.deltaTime);
            //movetoward
            Vector3 movement = transform.TransformDirection(new Vector3(0, -1, 1 * mov_stat.speed * Time.deltaTime));
            charactercontroller.Move(movement);
        }
        void lockon()
        {
            Vector3 lookpos = enemy.transform.position - transform.position;
            lookpos.y = 0;
            Quaternion desirerotation = Quaternion.LookRotation(lookpos);
            transform.rotation = Quaternion.Slerp(transform.rotation, desirerotation, mov_stat.rotation_speed * Time.deltaTime);
        }
        node Currentnode
        {
            get { if (transformnode) return transformnode.Currentnode(); return null; }
        }
        node BattlegroundNode
        {
            get { if (battleground) return battleground.battleground; return null; }
        }
        bool issearchcomplete
        {
            get { return BattlegroundNode.IsSearchcomplete(); }
        }
        bool gotoenemy
        {
            get
            {
                return true;
            }
        }
        node next()
        {
            node leastfscore = Currentnode;
            foreach (node neightbor in leastfscore.Neightbor)
            {
                if (fscore[neightbor] < fscore[leastfscore])
                {
                    leastfscore = neightbor;
                }
            }
            return leastfscore;
        }
        bool isenemyinmeleerange()
        {
            count = 0;
            foreach (GameObject i in battleground.Enemiesofenemy)
            {
                if (i.GetComponent<Collider>().bounds.SqrDistance(transform.position) < 1f)
                {
                    enemiesinrange[count] = i;
                    count++;if (count == enemiesinrange.Length) break;
                }
            }
            return count > 0;
        }
        bool isenemyinrangeattack()
        {
            count = 0;
            foreach (GameObject i in battleground.Enemiesofenemy)
            {
                if (i.GetComponent<Collider>().bounds.SqrDistance(transform.position) < 20f)
                {
                    enemiesinrange[count] = i;
                    count++; if (count == enemiesinrange.Length) break;
                }
            }
            return count > 0;
        }
        GameObject enemy
        {
            get{
                if(count>0)
                {
                    return enemiesinrange[0];
                }
                return battleground.Enemyofenemy(gameObject);
            }
        }
        Dictionary<node, int> fscore
        {
            get { return BattlegroundNode.Fscore(); }
        }
        public override void BeginBehaviour()
        {
            if (battleground == null)
            {
                battleground = BattleGround.CurrentBattleGround;
            }
        }
        public override void UpdateBehaviour()
        {
            if (Currentnode && BattlegroundNode)
            {
                if (Currentnode != BattlegroundNode)
                {
                    if (issearchcomplete)
                    {
                        move();
                        animator.SetInteger("Behaviour", Run);//assaultbattleground
                    }
                    else
                    {
                        animator.SetInteger("Behaviour", Stop);//wait
                    }
                }
                else
                {
                    if (isenemyinmeleerange())
                    {
                        if(count==enemiesinrange.Length)
                        {
                            animator.SetInteger("Behaviour", MassAttack);
                        }
                        else
                        {
                            lockon();
                            animator.SetInteger("Behaviour", Attack);
                        }
                    }
                    else
                    {
                        if(isenemyinrangeattack())
                        {
                            lockon();
                            animator.SetInteger("Behaviour", RangeAttack);
                        }
                        else
                        {
                            lockon();
                            animator.SetInteger("Behaviour", Stop);
                        }
                    }
                }
            }
            else
            {
                animator.SetInteger("Behaviour", Stop);
            }
        }
        public override void EndBehaviour()
        {
        }

    }
}
