﻿using UnityEngine;
using BAYUAjiStudio.Stat;
namespace BAYUAjiStudio.AI.Behaviour.Neutral
{
    /// <summary>
    /// basic neutral behaviour
    /// look at player if player near 
    /// </summary>
    public class Hold : ArtificialBehaviour
    {
        CharacterController charactercontroller;
        movement_stat mov_stat;
        Animator animator;
        PlayerLister playerlister;
        GameObject player;
        Quaternion defaultrotation;

        public int Idle;
        public int Lockon;

        void Start()
        {
            charactercontroller = GetComponent<CharacterController>();
            mov_stat = GetComponent<movement_stat>();
            animator = GetComponent<Animator>();
            playerlister = FindObjectOfType<PlayerLister>();
            defaultrotation = transform.rotation;
        }
        void Update()
        {
            
        }
        void lockon()
        {
            Vector3 lookpos = player.transform.position - transform.position;
            lookpos.y = 0;
            Quaternion desirerotation = Quaternion.LookRotation(lookpos);
            transform.rotation = Quaternion.Slerp(transform.rotation, desirerotation, mov_stat.rotation_speed * Time.deltaTime);
        }
        void backtoidle()
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, defaultrotation, mov_stat.rotation_speed * Time.deltaTime);
        }
        GameObject[] Players
        {
            get
            {
                return playerlister.PlayerList();
            }
        }
        bool playernearest
        {
            get
            {
                foreach(GameObject i in Players)
                {
                    if(i.GetComponent<Collider>().bounds.SqrDistance(transform.position)<1f)
                    {
                        player = i;
                        return true;
                    }
                }
                return false;
            }
        }
        public override void BeginBehaviour()
        {
            
        }
        public override void UpdateBehaviour()
        {
            if(playernearest)
            {
                lockon();
                animator.SetInteger("Behaviour", Lockon);
            }
            else
            {
                backtoidle();
                animator.SetInteger("Behaviour", Idle);
            }
        }
        public override void EndBehaviour()
        {
            
        }
    }
}