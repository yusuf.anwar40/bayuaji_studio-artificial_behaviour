﻿using UnityEngine;
using BAYUAjiStudio.AI.Pathfinding;
using BAYUAjiStudio.Stat;
namespace BAYUAjiStudio.AI.Behaviour.Neutral
{
    /// <summary>
    /// different from pedestrian is interarea
    /// </summary>
    public class Pedestrian9 : ArtificialBehaviour
    {
        CharacterController charactercontroller;
        movement_stat mov_stat;
        Animator animator;
        Transformnode transformnode;
        PlayerLister playerlister;
        int nextint=1;

        public node[] waypoint;
        public int Walk;
        public int Stop;
        void Start()
        {
            charactercontroller = GetComponent<CharacterController>();
            mov_stat = GetComponent<movement_stat>();
            animator = GetComponent<Animator>();
            transformnode = GetComponent<Transformnode>();
            playerlister = FindObjectOfType<PlayerLister>();
        }
        void Update()
        {

        }
        void walk()
        {
            Vector3 lookpos = next().transform.position - transform.position;
            lookpos.y = 0;
            Quaternion desirerotation = Quaternion.LookRotation(lookpos);
            transform.rotation = Quaternion.Slerp(transform.rotation, desirerotation, mov_stat.rotation_speed * Time.deltaTime);
            //movetoward
            Vector3 movement = transform.TransformDirection(new Vector3(0, -1, 1 * mov_stat.speed * Time.deltaTime));
            charactercontroller.Move(movement);
        }
        bool ispathblockedbyplayer
        {
            get
            {
                Vector3 tonextwaypoint = (next().transform.position - transform.position).normalized;
                foreach (GameObject i in Playerlist)
                {
                    Vector3 topathblocker = (i.transform.position - transform.position).normalized;
                    if (Vector3.Dot(topathblocker, tonextwaypoint) > 0.6f)
                    {
                        if (i.GetComponent<Collider>().bounds.SqrDistance(transform.position) < 1f)
                        {
                            return true;
                        }
                    }
                }
                return false;
            }
        }
        GameObject[] Playerlist
        {
            get
            {
                return playerlister.PlayerList();
            }
        }
        node Currentnode
        {
            get { if (transformnode) return transformnode.Currentnode(); return null; }
        }
        node next()
        {
            if(Currentnode==waypoint[nextint])
            {
                nextint++;
                if (nextint == waypoint.Length)
                {
                    nextint = 0;
                }
            }
            return waypoint[nextint];
        }
        public override void BeginBehaviour()
        {
            transform.position = waypoint[0].transform.position;
        }
        public override void UpdateBehaviour()
        {
            if (Currentnode)
            {
                if (waypoint.Length > 1)
                {
                    if (ispathblockedbyplayer)
                    {
                        animator.SetInteger("Behaviour", Stop);
                    }
                    else
                    {
                        walk();
                        animator.SetInteger("Behaviour", Walk);
                    }
                }
                else
                {
                    animator.SetInteger("Behaviour", Stop);
                }
            }
            else
            {
                animator.SetInteger("Behaviour", Stop);
            }
        }
        public override void EndBehaviour()
        {

        }
    }
}