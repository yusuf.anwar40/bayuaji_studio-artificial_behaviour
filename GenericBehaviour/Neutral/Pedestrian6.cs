﻿using UnityEngine;
using BAYUAjiStudio.Stat;
namespace BAYUAjiStudio.AI.Behaviour.Neutral
{
    /// <summary>
    /// same as pedestrian5 but reset to firstposition
    /// </summary>
    public class Pedestrian6 : ArtificialBehaviour
    {
        CharacterController charactercontroller;
        movement_stat mov_stat;
        Animator animator;
        PlayerLister playerlister;
        int nextint = 1;

        public Transform Lane;
        public int Walk;
        public int Stop;
        void Start()
        {
            charactercontroller = GetComponent<CharacterController>();
            mov_stat = GetComponent<movement_stat>();
            animator = GetComponent<Animator>();
            playerlister = FindObjectOfType<PlayerLister>();
        }
        void Update()
        {

        }
        void walk()
        {
            Vector3 lookpos = next().transform.position - transform.position;
            lookpos.y = 0;
            Quaternion desirerotation = Quaternion.LookRotation(lookpos);
            transform.rotation = Quaternion.Slerp(transform.rotation, desirerotation, mov_stat.rotation_speed * Time.deltaTime);
            //movetoward
            Vector3 movement = transform.TransformDirection(new Vector3(0, -1, 1 * mov_stat.speed * Time.deltaTime));
            charactercontroller.Move(movement);
        }
        bool ispathblockedbyplayer
        {
            get
            {
                if (Vector3.Distance(transform.position, next().transform.position) < 0.5f)
                {
                    nextint++;
                    if (nextint == Lane.childCount)
                    {
                        nextint = 1;
                        Vector3 lookpos = Lane.GetChild(1).transform.position - Lane.GetChild(0).transform.position;
                        Quaternion desirerotation = Quaternion.LookRotation(lookpos);
                        transform.position = Lane.GetChild(0).transform.position;
                        transform.rotation = desirerotation;
                    }
                }
                foreach (GameObject i in Playerlist)
                {
                    Vector3 playertoself = transform.position - i.transform.position;
                    Vector3 playertonext = next().transform.position - i.transform.position;
                    if (Vector3.Angle(playertoself, playertonext) < 150f)
                    {
                        if (i.GetComponent<Collider>().bounds.SqrDistance(transform.position) < 1f)
                        {
                            return true;
                        }
                    }
                }
                return false;
            }
        }
        GameObject[] Playerlist
        {
            get
            {
                return playerlister.PlayerList();
            }
        }
        Transform next()
        {
            return Lane.GetChild(nextint);
        }
        public override void BeginBehaviour()
        {
            transform.position = Lane.GetChild(0).position;
        }
        public override void UpdateBehaviour()
        {
            if (Lane.childCount > 1)
            {
                if (ispathblockedbyplayer)
                {
                    animator.SetInteger("Behaviour", Stop);//stop to talk
                }
                else
                {
                    walk();
                    animator.SetInteger("Behaviour", Walk);
                }
            }
            else
            {
                animator.SetInteger("Behaviour", Stop);
            }
        }
        public override void EndBehaviour()
        {

        }
    }
}