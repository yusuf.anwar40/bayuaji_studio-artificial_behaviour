﻿using UnityEngine;
using BAYUAjiStudio.Stat;
namespace BAYUAjiStudio.AI.Behaviour.Neutral
{
    /// <summary>
    /// pedestrian6 but add avoidance
    /// </summary>
    public class Pedestrian8 : ArtificialBehaviour
    {
        CharacterController charactercontroller;
        movement_stat mov_stat;
        Animator animator;
        PlayerLister playerlister;
        Vector3 clockwisetarget;
        Vector3 counterclockwisetarget;
        GameObject pathblocker;
        int nextint = 1;

        public Transform Lane;
        public int Walk;
        public int Stop;
        void Start()
        {
            charactercontroller = GetComponent<CharacterController>();
            mov_stat = GetComponent<movement_stat>();
            animator = GetComponent<Animator>();
            playerlister = FindObjectOfType<PlayerLister>();
        }
        void Update()
        {

        }
        void walk()
        {
            Vector3 lookpos = next().transform.position - transform.position;
            lookpos.y = 0;
            Quaternion desirerotation = Quaternion.LookRotation(lookpos);
            transform.rotation = Quaternion.Slerp(transform.rotation, desirerotation, mov_stat.rotation_speed * Time.deltaTime);
            //movetoward
            Vector3 movement = transform.TransformDirection(new Vector3(0, -1, 1 * mov_stat.speed * Time.deltaTime));
            charactercontroller.Move(movement);
        }
        void circlearoundenemyclockwise()
        {
            Vector3 target = clockwisetarget;
            Vector3 lookpos = target - transform.position;
            lookpos.y = 0;

            Quaternion desirerotation = Quaternion.LookRotation(lookpos);
            transform.rotation = Quaternion.Slerp(transform.rotation, desirerotation, mov_stat.rotation_speed * Time.deltaTime);
            //movetoward
            Vector3 movement = transform.TransformDirection(new Vector3(0, -1, 1 * mov_stat.speed * Time.deltaTime));
            charactercontroller.Move(movement);
        }
        void circlearoundenemycounterclockwise()
        {
            Vector3 target = counterclockwisetarget;
            Vector3 lookpos = target - transform.position;
            lookpos.y = 0;

            Quaternion desirerotation = Quaternion.LookRotation(lookpos);
            transform.rotation = Quaternion.Slerp(transform.rotation, desirerotation, mov_stat.rotation_speed * Time.deltaTime);
            //movetoward
            Vector3 movement = transform.TransformDirection(new Vector3(0, -1, 1 * mov_stat.speed * Time.deltaTime));
            charactercontroller.Move(movement);
        }
        bool isnearplayer
        {
            get
            {
                foreach (GameObject i in Playerlist)
                {
                    if (i.GetComponent<Collider>().bounds.SqrDistance(transform.position) < 1f)
                    {
                        pathblocker = i;
                        return true;
                    }
                }
                return false;
            }
        }
        bool ispathblockedbyplayer
        {
            get
            {
                if (Vector3.Distance(transform.position, next().transform.position) < 0.5f)
                {
                    nextint++;
                    if (nextint == Lane.childCount)
                    {
                        nextint = 1;
                        Vector3 lookpos = Lane.GetChild(1).transform.position - Lane.GetChild(0).transform.position;
                        Quaternion desirerotation = Quaternion.LookRotation(lookpos);
                        transform.position = Lane.GetChild(0).transform.position;
                        transform.rotation = desirerotation;
                    }
                }
                foreach (GameObject i in Playerlist)
                {
                    Vector3 playertoself = transform.position - i.transform.position;
                    Vector3 playertonext = next().transform.position - i.transform.position;
                    if (Vector3.Angle(playertoself, playertonext) < 150f)
                    {
                        pathblocker = i;
                        return true;
                    }
                }
                return false;
            }
        }
        bool isclockwiseeficient()
        {
            Vector3 pathblockertowaypoint = (next().transform.position - pathblocker.transform.position).normalized;
            Vector3 clockwise = new Vector3(pathblockertowaypoint.z, pathblockertowaypoint.y, -pathblockertowaypoint.x);
            Vector3 counterclockwise = new Vector3(-pathblockertowaypoint.z, pathblockertowaypoint.y, pathblockertowaypoint.x);

            clockwisetarget = pathblocker.transform.position + clockwise;

            counterclockwisetarget = pathblocker.transform.position + counterclockwise;

            float clockwisedistance = Vector3.Distance(transform.position, clockwisetarget);
            float counterclockwisedistance = Vector3.Distance(transform.position, counterclockwisetarget);

            return clockwisedistance < counterclockwisedistance;
        }
        GameObject[] Playerlist
        {
            get
            {
                return playerlister.PlayerList();
            }
        }
        Transform next()
        {
            return Lane.GetChild(nextint);
        }
        public override void BeginBehaviour()
        {
            transform.position = Lane.GetChild(0).position;
        }
        public override void UpdateBehaviour()
        {
            if (isnearplayer)
            {
                animator.SetInteger("Behaviour", Stop);
            }
            else
            {
                if (Lane.childCount > 1)
                {
                    if (ispathblockedbyplayer)
                    {
                        if (isclockwiseeficient())
                        {
                            circlearoundenemyclockwise();
                            animator.SetInteger("Behaviour", Walk);
                        }
                        else
                        {
                            circlearoundenemycounterclockwise();
                            animator.SetInteger("Behaviour", Walk);
                        }
                    }
                    else
                    {
                        walk();
                        animator.SetInteger("Behaviour", Walk);
                    }
                }
                else
                {
                    animator.SetInteger("Behaviour", Stop);
                }
            }
        }
        public override void EndBehaviour()
        {

        }
    }
}