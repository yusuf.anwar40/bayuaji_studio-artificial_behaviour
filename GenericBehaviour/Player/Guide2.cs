﻿using System.Collections.Generic;
using UnityEngine;
using BAYUAjiStudio.AI.Pathfinding;
using BAYUAjiStudio.Stat;
namespace BAYUAjiStudio.AI.Behaviour
{
    /// <summary>
    /// this behavior to guide player.
    /// different from Guide behaviour is Guide2 behaviour is guide All player
    ///</summary>
    public class Guide2 : ArtificialBehaviour
    {
        CharacterController charactercontroller;
        movement_stat mov_stat;
        Animator animator;

        Transformnode transformnode;
        node Goalnode;

        GameObject[] targetstoguide;
        Transformnode targettoguidetransformnode;
        int outsidecurrentnodecount;

        public int Run;
        public int Stop;
        public int Search;

        //public GameObject experiment;
        //public node experimentgoal;
        void Start()
        {
            charactercontroller = GetComponent<CharacterController>();
            mov_stat = GetComponent<movement_stat>();
            animator = GetComponent<Animator>();
            transformnode = GetComponent<Transformnode>();
        }
        void movetogoalnode()
        {
            Vector3 lookpos = nextforgoal().transform.position - transform.position;
            lookpos.y = 0;
            Quaternion desirerotation = Quaternion.LookRotation(lookpos);
            transform.rotation = Quaternion.Slerp(transform.rotation, desirerotation, mov_stat.rotation_speed * Time.deltaTime);
            //movetoward
            Vector3 movement = transform.TransformDirection(new Vector3(0, -1, 1 * mov_stat.speed * Time.deltaTime));
            charactercontroller.Move(movement);
        }
        void movetofindtarget()
        {
            Vector3 lookpos = nextfortargetoguide().transform.position - transform.position;
            lookpos.y = 0;
            Quaternion desirerotation = Quaternion.LookRotation(lookpos);
            transform.rotation = Quaternion.Slerp(transform.rotation, desirerotation, mov_stat.rotation_speed * Time.deltaTime);
            //movetoward
            Vector3 movement = transform.TransformDirection(new Vector3(0, -1, 1 * mov_stat.speed * Time.deltaTime));
            charactercontroller.Move(movement);
        }
        void blindsearch()
        {
            TargettoGuideCurrentnode.blindsearch();
            Goalnode.blindsearch();
        }
        node nextfortargetoguide()
        {
            node leastfscore = Currentnode;
            foreach (node neightbor in leastfscore.Neightbor)
            {
                if (targettoguidefscore[neightbor] < targettoguidefscore[leastfscore])
                {
                    leastfscore = neightbor;
                }
            }
            return leastfscore;
        }
        node nextforgoal()
        {
            node leastfscore = Currentnode;
            foreach (node neightbor in leastfscore.Neightbor)
            {
                if (goalfscore[neightbor] < goalfscore[leastfscore])
                {
                    leastfscore = neightbor;
                }
            }
            return leastfscore;
        }
        node Currentnode
        {
            get { if (transformnode) return transformnode.Currentnode(); return null; }
        }
        node TargettoGuideCurrentnode
        {
            get
            {
                if (targettoguidetransformnode)
                    return targettoguidetransformnode.Currentnode();
                return null;
            }
        }
        Dictionary<node, int> goalfscore
        {
            get { return Goalnode.Fscore(); }
        }
        Dictionary<node, int> targettoguidefscore
        {
            get { return TargettoGuideCurrentnode.Fscore(); }
        }
        bool issearchcomplete
        {
            get
            {
                return Goalnode.IsSearchcomplete() && TargettoGuideCurrentnode.IsSearchcomplete();
            }
        }
        bool targetinneightborarea
        {
            get
            {
                if (Currentnode.Neightbor.Length == 0) return true;
                foreach (node i in Currentnode.Neightbor)
                {
                    if (i == TargettoGuideCurrentnode)
                    {
                        return true;
                    }
                }
                return false;
            }
        }
        bool isalltargettoguideoutsidecurrentnode
        {
            get
            {
                outsidecurrentnodecount = 0;
                foreach (GameObject i in targetstoguide)
                {
                    targettoguidetransformnode = i.GetComponent<Transformnode>();
                    if (targettoguidetransformnode.Currentnode() != Currentnode)
                    {
                        outsidecurrentnodecount++;
                    }
                }
                return outsidecurrentnodecount == targetstoguide.Length;
            }
        }
        bool isalltargetstoguideinsidecurrentnode
        {
            get
            {
                return outsidecurrentnodecount == 0;
            }
        }
        public void SetGoal(node goal)
        {
            Goalnode = goal;
        }
        public override void BeginBehaviour()
        {
            targetstoguide = GameObject.FindGameObjectsWithTag("Player");
            animator.SetInteger("Behaviour", Stop);
        }
        public override void UpdateBehaviour()
        {
            if (Currentnode && Goalnode)
            {
                if (targetstoguide.Length>0)
                {
                    if (Goalnode.IsSearchcomplete())
                    {
                        if (isalltargettoguideoutsidecurrentnode)
                        {
                            movetofindtarget();
                            animator.SetInteger("Behaviour", Run);
                        }
                        else
                        {
                            if(isalltargetstoguideinsidecurrentnode)
                            {
                                if (goalfscore[Currentnode] == 0)
                                {
                                    animator.SetInteger("Behaviour", Stop);//stop when it is goal
                                }
                                else
                                {
                                    movetogoalnode();
                                    animator.SetInteger("Behaviour", Run);//guide player
                                }
                            }
                            else
                            {
                                animator.SetInteger("Behaviour", Stop);//wait for player
                            }
                        }
                    }
                    else
                    {
                        blindsearch();
                        animator.SetInteger("Behaviour", Search);//pathplanning
                    }
                }
                else
                {
                    animator.SetInteger("Behaviour", Stop);//no player are be guided
                }
            }
            else
            {
                animator.SetInteger("Behaviour", Stop);//no goal
            }
        }
        public override void EndBehaviour()
        {
        }
    }
}
