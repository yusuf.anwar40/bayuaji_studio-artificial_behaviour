﻿using System;
using System.Collections.Generic;
using UnityEngine;
using BAYUAjiStudio.AI.Pathfinding;
using BAYUAjiStudio.Stat;
namespace BAYUAjiStudio.AI.Behaviour
{
    /// <summary>
    /// alternate name ItemGatherer
    /// </summary>
    public class Follow2 : ArtificialBehaviour
    {
        CharacterController charactercontroller;
        movement_stat mov_stat;
        Animator animator;
        GameObject[] pickableitems;
        GameObject pickableitem;

        public GameObject TargetToFollow;
        public Vector3 localpositiontofollow;

        Transformnode transformnode;
        Transformnode target_transformnode;
        float maximumfollowdistance;

        public int Run;
        public int Stop;
        public int pickitem;
        void Start()
        {
            charactercontroller = GetComponent<CharacterController>();
            mov_stat = GetComponent<movement_stat>();
            animator = GetComponent<Animator>();
            transformnode = GetComponent<Transformnode>();
            maximumfollowdistance = localpositiontofollow.sqrMagnitude + 1f;
        }
        void Update()
        {
            if (Targetnode)
            {
                if (!issearchcomplete)
                {
                    blindsearch();
                }
            }
        }
        void move()
        {
            Vector3 lookpos = next().transform.position - transform.position;
            lookpos.y = 0;
            Quaternion desirerotation = Quaternion.LookRotation(lookpos);
            transform.rotation = Quaternion.Slerp(transform.rotation, desirerotation, mov_stat.rotation_speed * Time.deltaTime);
            //movetoward
            Vector3 movement = transform.TransformDirection(new Vector3(0, -1, 1 * mov_stat.speed * Time.deltaTime));
            charactercontroller.Move(movement);
        }
        void movetotargettofollow()
        {
            Vector3 followpos = TargetToFollow.transform.rotation * localpositiontofollow;
            Vector3 globalpositiontofollow = (TargetToFollow.transform.position + followpos);
            globalpositiontofollow = Currentnode.bounding(globalpositiontofollow);
            Vector3 lookpos = globalpositiontofollow - transform.position;//temp
            lookpos.y = 0;
            Quaternion desirerotation = Quaternion.LookRotation(lookpos, Vector3.up);
            transform.rotation = Quaternion.Slerp(transform.rotation, desirerotation, mov_stat.rotation_speed * Time.deltaTime);
            //movetoward
            Vector3 movement = transform.TransformDirection(new Vector3(0, -1, 1 * mov_stat.speed * Time.deltaTime));
            charactercontroller.Move(movement);
        }
        void movetopickableitem()
        {
            Vector3 lookpos = pickableitem.transform.position - transform.position;
            lookpos.y = 0;
            Quaternion desirerotation = Quaternion.LookRotation(lookpos);
            transform.rotation = Quaternion.Slerp(transform.rotation, desirerotation, mov_stat.rotation_speed * Time.deltaTime);
            //movetoward
            Vector3 movement = transform.TransformDirection(new Vector3(0, -1, 1 * mov_stat.speed * Time.deltaTime));
            charactercontroller.Move(movement);
        }
        void pickitemvoid()
        {
            pickableitem.SetActive(false);//temp
            pickableitem = null;//temp
        }
        node next()
        {
            node leastfscore = Currentnode;
            foreach (node neightbor in leastfscore.Neightbor)
            {
                if (fscore[neightbor] < fscore[leastfscore])
                {
                    leastfscore = neightbor;
                }
            }
            return leastfscore;
        }
        Dictionary<node, int> fscore
        {
            get { return Targetnode.Fscore(); }
        }
        void blindsearch()
        {
            Targetnode.blindsearch();
        }
        node Currentnode
        {
            get
            {
                if (transformnode) return transformnode.Currentnode();
                return null;
            }
        }
        node Targetnode
        {
            get
            {
                if (TargetToFollow)
                {
                    return TargetToFollow.GetComponent<Transformnode>().Currentnode();
                }
                return null;
            }
        }
        float TargetDistance
        {
            get
            {
                return TargetToFollow.GetComponent<Collider>().bounds.SqrDistance(transform.position);
            }
        }
        bool issearchcomplete
        {
            get { return Targetnode.IsSearchcomplete(); }
        }
        bool TargettoFollowinRange
        {
            get { return TargetDistance < maximumfollowdistance; }
        }
        bool isthereanypickableiteminarea
        {
            get
            {
                if(pickableitem)
                {
                    return true;
                }
                pickableitems = GameObject.FindGameObjectsWithTag("pickableitem");
                foreach (GameObject i in pickableitems)
                {
                    if (Currentnode.inarea(i.transform.position))
                    {
                        pickableitem = i;
                        return true;
                    }
                }
                return false;
            }
        }
        bool onpickableitem
        {
            get
            {
                return Vector3.Distance(pickableitem.transform.position, transform.position) < 1f;
            }
        }
        public override void BeginBehaviour()
        {
            
        }
        public override void UpdateBehaviour()
        {
            if (Currentnode && Targetnode)
            {
                if (isthereanypickableiteminarea)
                {
                    if (onpickableitem)
                    {
                        pickitemvoid();
                        animator.SetInteger("Behaviour", pickitem);
                    }
                    else
                    {
                        movetopickableitem();
                        animator.SetInteger("Behaviour", Run);
                    }
                }
                else
                {
                    if (issearchcomplete)
                    {
                        if (Currentnode == Targetnode)
                        {

                            if (TargettoFollowinRange)
                            {
                                animator.SetInteger("Behaviour", Stop);
                            }
                            else
                            {
                                movetotargettofollow();
                                animator.SetInteger("Behaviour", Run);
                            }
                        }
                        else
                        {
                            move();
                            animator.SetInteger("Behaviour", Run);
                        }
                    }
                    else
                    {
                        animator.SetInteger("Behaviour", Stop);
                    }
                }
            }
            else
            {
                animator.SetInteger("Behaviour", Stop);
            }
        }
        public override void EndBehaviour()
        {

        }
        public void SetTargettoFollow(GameObject target)
        {
            TargetToFollow = target;
        }
    }
}
