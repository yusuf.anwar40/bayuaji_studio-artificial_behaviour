﻿using UnityEngine;
namespace BAYUAjiStudio.AI.Behaviour
{
    public class Hold2:ArtificialBehaviour
    {
        Animator animator;
        GameObject TwinGameObject;

        public int Stop;
        void Start()
        {
            animator = GetComponent<Animator>();
        }
        public override void BeginBehaviour()
        {
            TwinGameObject = GameObject.Find(gameObject.name + " (1)");//temp
            TwinGameObject.SetActive(false);
            transform.SetPositionAndRotation(TwinGameObject.transform.position,TwinGameObject.transform.rotation);
            animator.SetInteger("Behaviour", Stop);
        }
        public override void UpdateBehaviour()
        {
            animator.SetInteger("Behaviour", Stop);
        }
        public override void EndBehaviour()
        {
            
        }
    }
}
