﻿using System.Collections.Generic;
using UnityEngine;

namespace BAYUAjiStudio.AI.Pathfinding
{
    public class node: MonoBehaviour
    {
        Dictionary<node, int> fscore;
        Queue<node> unprocessednode;
        Collider area;
        bool issearchcomplete;

        public node[] Neightbor;
        public float cost;

        void Start(){
            area = GetComponent<Collider>();
            fscore = new Dictionary<node, int>();
            unprocessednode = new Queue<node>();
            issearchcomplete = false;
            fscore.Add(this,0);
            unprocessednode.Enqueue(this);
        }
        public bool inarea(Vector3 point)
        {
            if(area)
            {
                return area.bounds.Contains(point);
            }
            return false;
        }
        public float distance(Vector3 point)
        {
            if(area)return area.bounds.SqrDistance(point);
            return (transform.position - point).magnitude;
        }
        public void blindsearch()
        {
            if (unprocessednode.Count > 0)
            {
                node current = unprocessednode.Dequeue();
                foreach (node neightbor in current.Neightbor)
                {
                    int tentative_fscore = fscore[current] + 1;
                    if (!fscore.ContainsKey(neightbor))
                    {
                        fscore.Add(neightbor, tentative_fscore);
                        unprocessednode.Enqueue(neightbor);
                    }
                    else if (tentative_fscore < fscore[neightbor])
                    {
                        fscore[neightbor] = tentative_fscore;
                        unprocessednode.Enqueue(neightbor);
                    }
                }
            }
            else
            {
                issearchcomplete = true;
            }
        }
        public void blindsearch(int iterationspeed)
        {
            int i = 0;
            while (unprocessednode.Count > 0 && i<iterationspeed)
            {
                node current = unprocessednode.Dequeue();
                foreach (node neightbor in current.Neightbor)
                {
                    int tentative_fscore = fscore[current] + 1;
                    if (!fscore.ContainsKey(neightbor))
                    {
                        fscore.Add(neightbor, tentative_fscore);
                        unprocessednode.Enqueue(neightbor);
                    }
                    else if (tentative_fscore < fscore[neightbor])
                    {
                        fscore[neightbor] = tentative_fscore;
                        unprocessednode.Enqueue(neightbor);
                    }
                }
                i++;
            }
            issearchcomplete = (unprocessednode.Count > 0);
        }
        public bool IsSearchcomplete()
        {
            return issearchcomplete;
        }
        public Dictionary<node, int> Fscore()
        {
            return fscore;
        }
        public Vector3 bounding(Vector3 point)
        {
            if(inarea(point))
            {
                return point;
            }
            return area.bounds.ClosestPoint(point);
        }
    }
}
