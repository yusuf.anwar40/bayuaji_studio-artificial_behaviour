﻿using UnityEngine;
using UnityEngine.Experimental.AI;
using Unity.Collections;
namespace BAYUAjiStudio.AI.Pathfinding
{
    public class Transformnode : MonoBehaviour
    {
        node currentnode;
        node nearest = null;
        node[] findcurrentnodearray;
        int findcurrentnodeiterated;
        void OnEnable()
        {
            findnearestcurrentnodeimmediately();
        }
        void Update()
        {
            if (currentnode)
            {
                updatecurrentnode();
            }
            else
            {
                findnearestcurrentnodeimmediately();
            }
        }
        void beginfindnearestnode()
        {
            findcurrentnodearray = FindObjectsOfType<node>();
            findcurrentnodeiterated = 0;
            nearest = null;
            currentnode = null;
        }
        void findnearestcurrentnode(int iterationspeed=1)
        {
            int j = 0;
            while(j<iterationspeed && findcurrentnodeiterated<findcurrentnodearray.Length)
            {
                node i = findcurrentnodearray[findcurrentnodeiterated];
                if (i.inarea(transform.position))
                {
                    nearest = i;
                    findcurrentnodeiterated = findcurrentnodearray.Length;
                    break;
                }
                if (nearest)
                {
                    if (i.distance(transform.position) < nearest.distance(transform.position))
                    {
                        nearest = i;
                    }
                }
                else
                {
                    nearest = i;
                }
                j++;findcurrentnodeiterated++;
            }
            if(findcurrentnodeiterated == findcurrentnodearray.Length)
            {
                currentnode = nearest;
            }
        }
        void findnearestcurrentnodeimmediately()
        {
            nearest = null;
            foreach (node i in FindObjectsOfType<node>())
            {
                if (i.inarea(transform.position))
                {
                    nearest = i;
                    break;
                }
                if (nearest)
                {
                    if (i.distance(transform.position) < nearest.distance(transform.position))
                    {
                        nearest = i;
                    }
                }
                else
                {
                    nearest = i;
                }
            }
            currentnode = nearest;
        }
        void updatecurrentnode()
        {
            if (currentnode.inarea(transform.position))
            {
                return;
            }
            nearest = currentnode;
            float nearestdistance = nearest.distance(transform.position);
            foreach (node neighbor in currentnode.Neightbor)
            {
                if (neighbor.inarea(transform.position))
                {
                    nearest = neighbor;
                    break;
                }
                float neightbordistance = neighbor.distance(transform.position);
                if (neightbordistance < nearestdistance)
                {
                    nearest = neighbor;
                    nearestdistance = neightbordistance;
                }
            }
            currentnode = nearest;
        }
        public node Currentnode()
        {
            return currentnode;
        }
        public void Setposition(Vector3 position)
        {
            transform.position = position;
            findnearestcurrentnodeimmediately();
        }
        public void Setposition(Vector3 position,node nodecoordinate)
        {
            transform.position = position;
            currentnode = nodecoordinate;
        }
        public void Setposition(node nodecoordinate)
        {
            transform.position = nodecoordinate.transform.position;
            currentnode = nodecoordinate;
        }
    }
}