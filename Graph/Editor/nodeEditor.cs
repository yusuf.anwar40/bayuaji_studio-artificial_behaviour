﻿using UnityEngine;
using UnityEditor;
namespace BAYUAjiStudio.AI.Pathfinding
{
    [CustomEditor(typeof(node))]
    [CanEditMultipleObjects]
    public class NodeEditor : Editor
    {
        SerializedObject neightborserializedObject;
        node targetToEdit
        {
            get
            {
                return (target as node);
            }
        }
        node candidateneightbor
        {
            get
            {
                return targetToEdit.Candidatenode;
            }
        }
        void onEnable()
        {

        }
        void addneightbor()
        {
            if (targetToEdit != candidateneightbor)
            {
                bool selfcontaincandidate = false;
                bool candidatecontainself = false;
                foreach (node i in targetToEdit.Neightbor)
                {
                    if (i == candidateneightbor)
                    {
                        selfcontaincandidate = true;
                        break;
                    }
                }
                foreach (node j in candidateneightbor.Neightbor)
                {
                    if (j == targetToEdit)
                    {
                        candidatecontainself = true;
                        break;
                    }
                }
                node[] temp;
                if (!selfcontaincandidate)
                {
                    temp = new node[targetToEdit.Neightbor.Length + 1];
                    targetToEdit.Neightbor.CopyTo(temp, 0);
                    temp[temp.Length - 1] = candidateneightbor;
                    targetToEdit.Neightbor = temp;
                }
                if (!candidatecontainself)
                {
                    temp = temp = new node[candidateneightbor.Neightbor.Length + 1];
                    candidateneightbor.Neightbor.CopyTo(temp, 0);
                    temp[temp.Length - 1] = targetToEdit;
                    candidateneightbor.Neightbor = temp;
                }
            }
            targetToEdit.Candidatenode = null;
        }
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            if (candidateneightbor)
            {
                if (GUILayout.Button("Add Neigtbor"))
                {
                    addneightbor();
                    neightborserializedObject = new SerializedObject(candidateneightbor);
                    neightborserializedObject.ApplyModifiedProperties();
                    serializedObject.ApplyModifiedProperties();
                }
            }
        }
    }
}
