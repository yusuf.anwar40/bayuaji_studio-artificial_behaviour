﻿using System;
using System.Collections.Generic;
using UnityEngine;
using BAYUAjiStudio.AI.Pathfinding;
namespace BAYUAjiStudio.Battle
{
    public class BattleGround : MonoBehaviour
    {
        static BattleGround currentbattleground;

        Dictionary<GameObject, int> PlayerIndices;
        Dictionary<GameObject, int> EnemyIndices;
        List<GameObject> players;

        public node battleground;
        public node snipinggrounds;
        public List<GameObject> Enemies;

        public List<GameObject> Enemiesofenemy
        {
            get
            {
                return players;
            }
        }
        public static BattleGround CurrentBattleGround
        {
            get { return currentbattleground; }
        }
        void Start()
        {
            setplayerindices();
            setenemyindices();
            StartBattle();//temp
        }
        void StartBattle()
        {
            currentbattleground = this;
        }
        void EndBattle()
        {
            currentbattleground = null;
        }
        void setplayerindices()
        {
            players = new List<GameObject>();
            players.AddRange(GameObject.FindGameObjectsWithTag("Player"));//temporary
            PlayerIndices = new Dictionary<GameObject, int>();
            for (int i = 0; i < players.Count; i++)
            {
                PlayerIndices.Add(players[i], i);
            }
        }
        void setenemyindices()
        {
            EnemyIndices = new Dictionary<GameObject, int>();
            for (int i = 0; i < Enemies.Count; i++)
            {
                EnemyIndices.Add(Enemies[i], i);
            }
        }
        void addplayer(GameObject player)
        {
            if (player.GetComponent<Transformnode>().Currentnode() == battleground)
            {
                players.Add(player);
                PlayerIndices.Add(player, players.Count - 1);
            }
        }
        void addenemy(GameObject enemy)
        {
            if(enemy.GetComponent<Transformnode>().Currentnode()==battleground)
            {
                Enemies.Add(enemy);
                EnemyIndices.Add(enemy,Enemies.Count-1);
            }
        }
        public void removeplayer(GameObject player)
        {
            swapplayer(PlayerIndices[player], PlayerIndices.Count - 1);
            players.RemoveAt(PlayerIndices.Count - 1);
            PlayerIndices.Remove(player);
            if(PlayerIndices.Count==0)
            {
                EndBattle();
            }
        }
        public void removeenemy(GameObject enemy)
        {
            swapenemy(EnemyIndices[enemy], EnemyIndices.Count - 1);
            Enemies.RemoveAt(EnemyIndices.Count - 1);
            EnemyIndices.Remove(enemy);
            if (EnemyIndices.Count == 0)
            {
                EndBattle();
            }
        }
        void swapplayer(int a, int b)
        {
            GameObject tempA = players[a]; GameObject tempB = players[b];
            PlayerIndices[tempA] = b; PlayerIndices[tempB] = a;
            players[a] = tempB; players[b] = tempA;
        }
        void swapenemy(int a, int b)
        {
            GameObject tempA = Enemies[a]; GameObject tempB = Enemies[b];
            EnemyIndices[tempA] = b; EnemyIndices[tempB] = a;
            Enemies[a] = tempB; Enemies[b] = tempA;
        }
        public GameObject Enemyofplayer(GameObject player)
        {
            if (!PlayerIndices.ContainsKey(player))
            {
                int enemyindex = player.GetHashCode();
                if (enemyindex < 0) enemyindex *= (-1);
                enemyindex = enemyindex % EnemyIndices.Count;
                return Enemies[enemyindex];
            }
            else
            {
                int enemyindex = PlayerIndices[player] % EnemyIndices.Count;
                return Enemies[enemyindex];
            }
        }
        public GameObject Enemyofenemy(GameObject enemy)
        {
            if (!EnemyIndices.ContainsKey(enemy))
            {
                int playerindex = enemy.GetHashCode();
                if (playerindex < 0) playerindex *= (-1);
                playerindex = playerindex % PlayerIndices.Count;
                return players[playerindex];
            }
            else
            {
                int playerindex = EnemyIndices[enemy] % PlayerIndices.Count;
                return players[playerindex];
            }
        }
    }
}
