﻿using System;
using UnityEngine;
namespace BAYUAjiStudio.AI.Behaviour
{
    public abstract class ArtificialBehaviour:MonoBehaviour
    {
        public abstract void BeginBehaviour();
        public abstract void UpdateBehaviour();
        public abstract void EndBehaviour();
    }
}
