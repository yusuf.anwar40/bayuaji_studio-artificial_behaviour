﻿using UnityEngine;
namespace BAYUAjiStudio.Stat
{
    public class PlayerLister:MonoBehaviour
    {
        GameObject[] playerlist;
        void Start()
        {
            playerlist =GameObject.FindGameObjectsWithTag("Player");
        }
        public GameObject[] PlayerList()
        {
            return playerlist;
        }
    }
}
