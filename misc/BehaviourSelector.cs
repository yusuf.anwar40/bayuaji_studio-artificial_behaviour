﻿using UnityEngine;
namespace BAYUAjiStudio.AI.Behaviour
{
    public class BehaviourSelector:MonoBehaviour
    {
        ArtificialBehaviour[] artificialBehaviours;
        public int selectedbehaviour;

        void Start()
        {
            artificialBehaviours = GetComponents<ArtificialBehaviour>();
            artificialBehaviours[selectedbehaviour].BeginBehaviour();
        }
        void Update()
        {
            artificialBehaviours[selectedbehaviour].UpdateBehaviour();
        }
        public void Setbehaviour(int index)
        {
            artificialBehaviours[selectedbehaviour].EndBehaviour();
            selectedbehaviour = index;
            artificialBehaviours[selectedbehaviour].BeginBehaviour();
        }
    }
}
